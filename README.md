# CSS

## Description

Simples projetos para aprender CSS

Simple projects to learn CSS

## Links
- [Play](https://master.d7zjquecsp1bd.amplifyapp.com);

## Tutorial
- [W3Schools](https://www.w3schools.com/css/);
- [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS);
